package com.belatrix.mvpsample.managers.network.requests

import com.belatrix.mvpsample.business.encryption.Crypto
import com.belatrix.mvpsample.managers.network.Endpoint
import com.belatrix.mvpsample.models.User
import io.reactivex.Observable
import java.util.*

/**
 * Created by hei on 18/01/18.
 * Handles the authentication request
 */
class LoginRequest(private val user: User) : Request<User.Uuid> {
    override fun execute(crypto: Crypto, endpoint: Endpoint): Observable<User.Uuid> {
        val params = HashMap<String, Any>()
        params["AccountName"] = crypto.apply(user.userId!!)
        params["Password"] = crypto.apply(user.password!!)
        return endpoint.login(params)
    }
}
