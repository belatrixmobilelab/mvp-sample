package com.belatrix.mvpsample.managers

import com.belatrix.mvpsample.managers.network.Endpoint
import com.belatrix.mvpsample.managers.network.requests.Request
import com.belatrix.mvpsample.business.encryption.Crypto
import com.belatrix.mvpsample.models.Response
import com.belatrix.mvpsample.utils.ErrorUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ltalavera on 25/11/17.
 * Handles the network communication
 */
@Singleton
class ApiClient @Inject constructor(private val endpoint: Endpoint, private var crypto: Crypto) {
    internal fun <T : Response> execute(request: Request<T>): Observable<T> {
        return request.execute(crypto, endpoint)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext{ throwable: Throwable ->
                    // Formats the error message
                    Observable.error(ErrorUtils.fromApi(throwable))
                }
                .flatMap { result ->
                    // Parse the error code and returns an exception if necessary
                    val error = ErrorUtils.fromResponse(result)
                    if (error == null) {
                        Observable.just(result)
                    } else {
                        Observable.error(error)
                    }
                }
    }
}