package com.belatrix.mvpsample.managers.network

import com.belatrix.mvpsample.models.User
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.*

/** Protocol version and paths  */
private const val PROTOCOL = "0.0.1"
private const val ADDRESS = "/request/"

/**
 * Created by ltalavera on 1/16/18.
 * All the requests to the Yodo Server
 */
interface Endpoint {
    @POST("$ADDRESS$PROTOCOL/login")
    fun login(@Body body: HashMap<String, Any>): Observable<User.Uuid>
}