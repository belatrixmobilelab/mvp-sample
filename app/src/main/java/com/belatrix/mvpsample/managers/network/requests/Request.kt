package com.belatrix.mvpsample.managers.network.requests

import com.belatrix.mvpsample.business.encryption.Crypto
import com.belatrix.mvpsample.managers.network.Endpoint
import io.reactivex.Observable

/**
 * Created by hei on 18/01/18.
 * basic request interface
 */
interface Request<T> {
    /**
     * Executes the request
     * @param crypto The system to encrypt the information
     * @param endpoint The api endpoint
     * @return The call required by the ApiClient
     */
    fun execute(crypto: Crypto, endpoint: Endpoint): Observable<T>
}