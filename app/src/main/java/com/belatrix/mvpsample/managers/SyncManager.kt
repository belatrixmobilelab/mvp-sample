package com.belatrix.mvpsample.managers

import com.belatrix.mvpsample.managers.ApiClient
import java.io.Serializable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by hei on 25/11/17.
 * Handles synchronization with the server
 */
@Singleton
class SyncManager @Inject constructor(private val apiClient: ApiClient) : Serializable {

}
