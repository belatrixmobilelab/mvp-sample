package com.belatrix.mvpsample.managers

import com.belatrix.mvpsample.helpers.PrefsHelper
import com.belatrix.mvpsample.managers.network.requests.LoginRequest
import com.belatrix.mvpsample.models.User
import io.reactivex.Observable
import java.io.Serializable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by hei on 25/11/17.
 * Handles user authentication
 */
@Singleton
class AuthManager @Inject constructor(private val apiClient: ApiClient) : Serializable {
    var user: User? = PrefsHelper.getUser(); private set
    private val listeners: MutableList<AuthStateListener> = ArrayList()

    fun addAuthStateListener(listener: AuthStateListener) {
        listeners.add(listener)
        listener.onAuthStateChanged(user)
    }

    fun removeAuthStateListener(listener: AuthStateListener) {
        listeners.remove(listener)
    }

    fun login(user: User): Observable<User> {
        return apiClient.execute(LoginRequest(user))
                .cache()
                .map {
                    user.apply {
                        this.userId = it.uuid
                        setUser(user)
                    }
                }
    }

    private fun setUser(user: User?): User? {
        if (this.user != user) {
            this.user = user
            PrefsHelper.setUser(this.user)

            for (listener in listeners) {
                listener.onAuthStateChanged(this.user)
            }
        }
        return this.user
    }

    interface AuthStateListener {
        fun onAuthStateChanged(user: User?)
    }
}