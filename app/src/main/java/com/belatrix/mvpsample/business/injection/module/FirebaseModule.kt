package com.belatrix.mvpsample.business.injection.module

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FirebaseModule {
    @Provides
    @Singleton
    fun providesFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Provides
    @Singleton
    fun providesPhoneProvider(auth: FirebaseAuth): PhoneAuthProvider {
        return PhoneAuthProvider.getInstance(auth)
    }
}