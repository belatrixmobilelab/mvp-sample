package com.belatrix.mvpsample.business.encryption.systems

import com.belatrix.mvpsample.business.encryption.Crypto
import com.belatrix.mvpsample.business.encryption.Crypto.Companion.REQ_SEP
import com.belatrix.mvpsample.business.encryption.ciphers.AES
import com.belatrix.mvpsample.business.encryption.ciphers.RSA

/**
 * Created by ltalavera on 1/19/18.
 * Implements the hybrid crypto-system
 */
class Hybrid : Crypto {
    override fun apply(vararg strings: String): String {
        // Generates an AES key
        val key = AES.generateKey()

        // Encrypts the AES key with RSA
        val encryptedKey = RSA.encrypt(AES.encodeKey(key))

        // Encrypts the data with the AES key
        val encryptedData = StringBuilder()
        for (plain in strings) {
            if (encryptedData.isNotEmpty()) {
                encryptedData.append(REQ_SEP)
            }
            encryptedData.append(AES.encrypt(plain, key))
        }

        // Returns the encrypted key and data, separated with a comma
        return encryptedKey + REQ_SEP + encryptedData.toString()
    }
}