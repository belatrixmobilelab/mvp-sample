package com.belatrix.mvpsample.business.injection.module

import com.belatrix.mvpsample.business.encryption.Crypto
import com.belatrix.mvpsample.business.encryption.systems.Hybrid
import com.belatrix.mvpsample.business.encryption.systems.None
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CryptoModule {
    @Provides
    @Singleton
    fun providesCryptoSystem(): Crypto {
        //return None()
        return Hybrid()
    }
}