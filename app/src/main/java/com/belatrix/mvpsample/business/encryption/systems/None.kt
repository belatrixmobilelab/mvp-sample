package com.belatrix.mvpsample.business.encryption.systems

import com.belatrix.mvpsample.business.encryption.Crypto
import com.belatrix.mvpsample.business.encryption.Crypto.Companion.REQ_SEP

/**
 * Created by ltalavera on 1/19/18.
 * No crypto-system used
 */
class None : Crypto {
    override fun apply(vararg strings: String): String {
        // Appends the data with a separator
        val data = StringBuilder()
        for (plain in strings) {
            if (data.isNotEmpty()) {
                data.append(REQ_SEP)
            }
            data.append(plain)
        }

        // Returns the unencrypted data
        return data.toString()
    }
}