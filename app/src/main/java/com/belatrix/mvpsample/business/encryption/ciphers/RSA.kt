package com.belatrix.mvpsample.business.encryption.ciphers

import com.belatrix.mvpsample.SampleApplication
import com.belatrix.mvpsample.utils.CryptUtils
import java.io.DataInputStream
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher

/**
 * Created by ltalavera on 1/18/18.
 * RSA implementation
 */
object RSA {
    /** Key and Cipher instance  */
    private const val KEY_INSTANCE = "RSA"
    private const val CIPHER_INSTANCE = "RSA/None/PKCS1Padding"

    /**
     * Public key generated with: openssl rsa -in 11.private.pem -pubout -outform DER -out 11.public.der
     * This key is created using the private key generated using openssl in unix environments
     */
    private val publicKey: PublicKey

    init {
        // Get context
        val context = SampleApplication.context

        // Get assets folder
        val `as` = context.assets
        val inFile = `as`.open("Keys/2048.public.der") // Set here the path
        val dis = DataInputStream(inFile)

        // Get key from assets
        val encodedKey = ByteArray(inFile.available())
        dis.readFully(encodedKey)
        dis.close()

        // Transform and set to public key
        val publicKeySpec = X509EncodedKeySpec(encodedKey)
        val kf = KeyFactory.getInstance(KEY_INSTANCE)
        publicKey = kf.generatePublic(publicKeySpec)
    }

    /**
     * Encrypts a string and transforms the byte array containing
     * the encrypted string to Hex format
     * @param plainText The unencrypted string
     * @return String   The encrypted string in Hex
     */
    fun encrypt(plainText: String): String {
        val cipher = Cipher.getInstance(CIPHER_INSTANCE)
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)
        val cipherData = cipher.doFinal(plainText.toByteArray(charset("UTF-8")))

        return CryptUtils.bytesToHex(cipherData)
    }
}