package com.belatrix.mvpsample.business.encryption

/**
 * Created by ltalavera on 1/19/18.
 * Interface for the different crypto-systems
 */
interface Crypto {
    companion object {
        internal const val REQ_SEP = ","
    }
    
    /**
     * Applies the algorithm to a plain text String
     * @param strings The input String
     * @return The output after the process
     */
    fun apply(vararg strings: String): String
}