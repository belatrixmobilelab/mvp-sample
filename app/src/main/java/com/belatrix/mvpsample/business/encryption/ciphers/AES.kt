package com.belatrix.mvpsample.business.encryption.ciphers

import com.belatrix.mvpsample.utils.CryptUtils
import java.security.SecureRandom
import javax.crypto.KeyGenerator
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.crypto.Cipher

/**
 * Created by ltalavera on 1/18/18.
 * AES implementation
 */
object AES {
    /** Key instance  */
    private const val KEY_INSTANCE = "AES"
    private const val KEY_SIZE = 128

    /** Cipher instance used for encryption  */
    private const val CIPHER_INSTANCE = "AES/CBC/PKCS5Padding"

    /**
     * Function that creates a key and returns the java object that contains it
     * @return PublicKey The public key specified in PUBLIC_KEY
     */
    fun generateKey(): SecretKeySpec {
        val keyGen = KeyGenerator.getInstance(KEY_INSTANCE)
        keyGen.init(KEY_SIZE, SecureRandom())
        val secretKey = keyGen.generateKey()

        return SecretKeySpec(secretKey.encoded, KEY_INSTANCE)
    }

    /**
     * Encodes the key to String (Base64)
     * @param key SecretKeySpec AES key
     * @return String Text version of the key
     */
    fun encodeKey(key: SecretKeySpec): String {
        return CryptUtils.bytesToHex(key.encoded)
    }

    /**
     * Generates the IV (salt) from the key
     * @param key The key used to encrypt
     * @return IvParameterSpec The IV (salt)
     */
    private fun generateIV(key: SecretKeySpec): IvParameterSpec {
        return IvParameterSpec(key.encoded)
    }

    /**
     * Encrypts a string and transforms the byte array containing
     * the encrypted string to Base64 format
     * @param plainText The unencrypted string
     * @param key The key used to encrypt the data
     * @return String The encrypted string in Base64
     */
    fun encrypt(plainText: String, key: SecretKeySpec): String {
        val cipher = Cipher.getInstance(CIPHER_INSTANCE)
        cipher.init(Cipher.ENCRYPT_MODE, key, generateIV(key))
        val cipherData = cipher.doFinal(plainText.toByteArray())

        return CryptUtils.bytesToHex(cipherData)
    }
}