package com.belatrix.mvpsample.business.injection.component

import android.app.Application
import com.belatrix.mvpsample.business.injection.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(
        modules=[
            AndroidSupportInjectionModule::class,
            ApplicationModule::class,
            ActivityBindingModule::class,
            FirebaseModule::class,
            ApiClientModule::class,
            CryptoModule::class
        ]
)
interface GraphComponent : AndroidInjector<DaggerApplication> {
    /**
     * Injects into the application class (must extend dagger application)
     * @param instance the dagger application class
     */
    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): GraphComponent
    }
}