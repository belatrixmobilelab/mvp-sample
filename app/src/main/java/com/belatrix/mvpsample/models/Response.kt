package com.belatrix.mvpsample.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by hei on 4/03/18.
 * Parses the server response
 */
@Suppress("unused")
@Root(name = "response", strict = false)
open class Response {
    @field:Element(name = "code")
    var code: String? = null

    @field:Element(name = "message", required = false)
    var message: String? = null
}