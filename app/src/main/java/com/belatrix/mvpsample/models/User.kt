package com.belatrix.mvpsample.models

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by hei on 11/12/17.
 * POJO for the User
 */
class User {
    /** Main attributes */
    var userId: String? = null
    var phoneNumber: String? = null
    var password: String? = null

    @Root(name = "response", strict = false)
    data class Uuid(
            @field:Element(name = "uuid", required = false)
            var uuid: String? = null
    ) : Response()
}