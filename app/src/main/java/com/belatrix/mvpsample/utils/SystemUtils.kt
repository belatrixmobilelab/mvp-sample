package com.belatrix.mvpsample.utils

import com.belatrix.mvpsample.SampleApplication

/**
 * Created by ltalavera on 1/29/18.
 * General utils for the system
 */
object SystemUtils {
    /**
     * Reads a file from the assets and returns the string content
     * @param fileName The name of the file
     * @return The content
     */
    fun readFromAssets(fileName: String): String {
        val inputStream = SampleApplication.context.assets.open(fileName)
        return inputStream.bufferedReader().use { it.readText() }
    }
}
