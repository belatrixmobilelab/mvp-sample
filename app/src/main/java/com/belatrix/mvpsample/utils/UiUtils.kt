package com.belatrix.mvpsample.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * Created by hei on 04/02/18.
 * Utils for graphic interface
 */
object UiUtils {
    /**
     * Hides the soft keyboard
     * @param view The current focus
     */
    fun hideSoftKeyboard(view: View?) {
        view?.let {
            val context = it.context
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }
}