package com.belatrix.mvpsample.utils

/**
 * Created by ltalavera on 1/18/18.
 * Common utils for the encryption
 */
object CryptUtils {
    /** Hexadecimal characters */
    private val HEX_CHARS = "0123456789abcdef".toCharArray()

    /**
     * Receives a byte array and returns a string of
     * hexadecimal numbers that represents it
     * @param bytes byte array
     * @return String of hexadecimal number
     */
    fun bytesToHex(bytes: ByteArray): String {
        val result = StringBuffer()

        bytes.forEach {
            val octet = it.toInt()
            val firstIndex = (octet and 0xF0).ushr(4)
            val secondIndex = octet and 0x0F
            result.append(HEX_CHARS[firstIndex])
            result.append(HEX_CHARS[secondIndex])
        }

        return result.toString()
    }

    /**
     * Transforms a String to bytes
     * @param str The String
     * @return bytes
     */
    fun hexToBytes(str: String): ByteArray {
        val len = str.length / 2
        val buffer = ByteArray(len)

        for (i in 0 until len) {
            buffer[i] = Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16).toByte()
        }

        return buffer
    }
}