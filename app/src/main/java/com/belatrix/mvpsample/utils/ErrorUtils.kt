package com.belatrix.mvpsample.utils

import com.belatrix.mvpsample.models.Response
import com.belatrix.mvpsample.R
import com.belatrix.mvpsample.SampleApplication
import com.belatrix.mvpsample.components.FirebaseAuthError
import com.belatrix.mvpsample.components.exceptions.ServerException
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.gson.stream.MalformedJsonException
import org.xmlpull.v1.XmlPullParserException
import timber.log.Timber
import java.io.IOException

/**
 * Created by ltalavera on 2/2/18.
 *  Handle errors and exceptions
 */
object ErrorUtils {
    private const val ERROR_FAILED = "00"

    /**
     * Gets the message for each one of the errors from firebase
     * @param ex The exception
     */
    fun fromFirebase(ex: FirebaseException): String {
        val context = SampleApplication.context

        if (ex !is FirebaseAuthException && ex !is FirebaseAuthInvalidCredentialsException) {
            return ex.localizedMessage
        }

        val error = FirebaseAuthError.fromException(ex as FirebaseAuthException)
        return when (error) {
            FirebaseAuthError.ERROR_INVALID_PHONE_NUMBER -> context.getString(R.string.error_phone_invalid)
            FirebaseAuthError.ERROR_TOO_MANY_REQUESTS -> context.getString(R.string.error_phone_attempts)
            FirebaseAuthError.ERROR_QUOTA_EXCEEDED -> context.getString(R.string.error_phone_quota)
            FirebaseAuthError.ERROR_INVALID_VERIFICATION_CODE -> context.getString(R.string.error_phone_confirmation)
            FirebaseAuthError.ERROR_SESSION_EXPIRED -> context.getString(R.string.error_phone_session_expired)
            else ->  error.description
        }
    }

    /**
     * Transforms exceptions adding map messages
     * @param error The exception
     * @return The exception with a predefined message
     */
    fun fromApi(error: Throwable): Exception {
        Timber.e(error)
        val context = SampleApplication.context
        return when (error) {
            is RuntimeException -> RuntimeException(context.getString(R.string.error_parsing))
            is MalformedJsonException -> MalformedJsonException(context.getString(R.string.error_parsing))
            is IOException -> IOException(context.getString(R.string.error_internet_connection))
            is NullPointerException -> NullPointerException(context.getString(R.string.error_internal_server))
            is XmlPullParserException -> XmlPullParserException(context.getString(R.string.error_parsing))
            else -> Exception(context.getString(R.string.error_server_unreachable))
        }
    }

    /**
     * Gets the error response code in case of an error code
     * @param response The response object with the parsed data
     */
    fun fromResponse(response: Response): ServerException? {
        val context = SampleApplication.context
        return when {
            response.code == ERROR_FAILED -> ServerException(context.getString(R.string.error_internal_server))
            // .. other respond errors
            else -> null
        }
    }
}