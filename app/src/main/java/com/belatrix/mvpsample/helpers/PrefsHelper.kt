package com.belatrix.mvpsample.helpers

import com.belatrix.mvpsample.models.User
import com.orhanobut.hawk.Hawk

/**
 * Created by hei on 18/01/18.
 * Helper for the application preferences using secure preferences
 */
object PrefsHelper {
    /** Keys for the app state */
    private const val PREF_PHONE_NUMBER = "PREF_PHONE_NUMBER"
    private const val PREF_USER = "PREF_USER"

    fun setPhoneNumber(phoneNumber: String?) {
        Hawk.put(PREF_PHONE_NUMBER, phoneNumber)
    }

    fun getPhoneNumber(): String? {
        return Hawk.get(PREF_PHONE_NUMBER)
    }

    fun setUser(user: User?) {
        Hawk.put(PREF_USER, user)
    }

    fun getUser(): User? {
        return Hawk.get(PREF_USER)
    }
}