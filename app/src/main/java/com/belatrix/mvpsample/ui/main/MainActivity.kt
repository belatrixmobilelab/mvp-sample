package com.belatrix.mvpsample.ui.main

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.belatrix.mvpsample.R
import com.belatrix.mvpsample.components.dialogs.MessageDialog
import com.belatrix.mvpsample.managers.AuthManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import javax.inject.Inject

class MainActivity : MainContract.View(), NavigationView.OnNavigationItemSelectedListener, MessageDialog.OnClickListener {
    @Inject lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.attach(this, lifecycle)
        prepareUI(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
            }
            R.id.nav_gallery -> {
            }
            R.id.nav_slideshow -> {
            }
            R.id.nav_manage -> {
            }
            R.id.nav_share -> {
            }
            R.id.nav_send -> {
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun loadCurrentFragment(currentTag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showText(text: String) {
        etCipher.text = text
    }

    override fun goToLogin() {
        finish()
    }

    override fun onClick(dialog: AlertDialog, responseCode: Int) {
        dialog.dismiss()
        Toast.makeText(this, "This is a toast message", Toast.LENGTH_LONG).show()
    }

    private fun prepareUI(savedInstanceState: Bundle?) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            MessageDialog.Builder(this)
                    .setTitle("Some title")
                    .setMessage("Some message")
                    .setPositiveButton("Ok", this)
                    .show(supportFragmentManager, TAG)
        }

        bGo.setOnClickListener{
            presenter.encrypt(etText.text.toString())
        }

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.text_drawer_open, R.string.text_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        if (savedInstanceState == null) {
            presenter.prepareCurrentFragment()
        }
    }
}
