package com.belatrix.mvpsample.ui.base

import android.arch.lifecycle.Lifecycle

/**
 * Created by hei on 25/11/17.
 * Every presenter must at least implement this interface
 */
interface Presenter<T : BaseView> {
    /**
     * Attach a new view to the presenter
     * @param view The view
     * @param lifecycle The lifecycle use to detach
     */
    fun attach(view: T, lifecycle: Lifecycle)

    /**
     * Removes the view reference from the presenter
     */
    fun detach()

    /**
     * Gets the attached view
     * @return The current view
     */
    fun getView(): T?

    /**
     * Gets the attached view or throws an exception
     * @return The current view
     */
    fun getViewOrThrow(): T

    /**
     * If the view is attached to the presenter
     * @return true if the view is not null
     */
    fun isViewAttached(): Boolean
}