package com.belatrix.mvpsample.ui.main

import com.belatrix.mvpsample.ui.base.BaseActivity
import com.belatrix.mvpsample.ui.base.BasePresenter

/**
 * Created by ltalavera on 3/14/18.
 * The contract for the main activity/presenter
 */
interface MainContract {
    abstract class View : BaseActivity() {
        /**
         * Loads the fragment defined to be displayed in the screen
         * @param currentTag The tag of the fragment to load
         */
        abstract fun loadCurrentFragment(currentTag: String)

        /**
         * Displays some text
         */
        abstract fun showText(text: String)

        /**
         * Finishes the current activity and starts the
         * Main Activity
         */
        abstract fun goToLogin()
    }

    abstract class Presenter<V : View> : BasePresenter<V>() {
        /**
         * Subscribe to the AuthListener
         */
        abstract fun subscribe()

        /**
         * Unsubscribe to the AuthListener
         */
        abstract fun unsubscribe()

        /**
         * Gets the current fragment based on the saved values
         */
        abstract fun prepareCurrentFragment()

        /**
         * Encrypts some text using a crypto system
         */
        abstract fun encrypt(text: String)
    }
}