package com.belatrix.mvpsample.ui.base

import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast
import com.belatrix.mvpsample.components.dialogs.DelayedProgressDialog
import com.belatrix.mvpsample.components.dialogs.MessageDialog
import com.belatrix.mvpsample.utils.UiUtils
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Created by hei on 30/01/18.
 * The base fragment with default functionality
 */
abstract class BaseFragment : BaseView, DaggerFragment() {
    companion object {
        internal var TAG = BaseFragment::class.java.simpleName
    }

    @Inject lateinit var progressDialog: DelayedProgressDialog

    override fun showProgress(show: Boolean) {
        UiUtils.hideSoftKeyboard(activity.currentFocus)

        if (show) {
            progressDialog.show(activity.supportFragmentManager, TAG)
        } else {
            progressDialog.cancel()
        }
    }

    override fun setProgressMessage(message: Int) {
        progressDialog.setMessage(message)
    }

    override fun showError(error: Int) {
        MessageDialog.Builder(activity)
                .setMessage(error)
                .show(activity.supportFragmentManager, TAG)
    }

    override fun showError(error: String?) {
        MessageDialog.Builder(activity)
                .setMessage(error)
                .show(activity.supportFragmentManager, TAG)
    }

    override fun toastError(error: Int) {
        activity.runOnUiThread { Toast.makeText(activity, error, Toast.LENGTH_SHORT).show() }
    }

    override fun snackError(error: String) {
        val root = activity.findViewById<View>(android.R.id.content)
        Snackbar.make(root, error, Snackbar.LENGTH_LONG).show()
    }
}