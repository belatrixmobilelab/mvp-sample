package com.belatrix.mvpsample.ui.phone

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.belatrix.mvpsample.R
import javax.inject.Inject

/**
 * Created by ltalavera on 3/16/18.
 * Implements the phone view
 */
class PhoneFragment : PhoneContract.View() {
    @Inject lateinit var presenter: PhonePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this, lifecycle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_phone, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareUI(savedInstanceState)
    }

    override fun showSubmitCode(phoneNumber: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setSubmitCode(code: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setValidatedUI() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPhoneNumber(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun prepareUI(savedInstanceState: Bundle?) {

    }
}