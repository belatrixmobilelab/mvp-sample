package com.belatrix.mvpsample.ui.base

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent

/**
 * Created by hei on 25/11/17.
 * Basic presenter for MVP
 */
abstract class BasePresenter<T : BaseView> : Presenter<T>, LifecycleObserver {
    private var view:T ? = null

    override fun attach(view: T, lifecycle: Lifecycle) {
        this.view = view
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun detach() {
        this.view = null
    }

    override fun getView() = view

    override fun getViewOrThrow() = getView() ?: throw IllegalStateException("view not attached")

    override fun isViewAttached() = view != null
}