package com.belatrix.mvpsample.ui.main

import android.arch.lifecycle.ViewModel
import com.belatrix.mvpsample.models.User
import io.reactivex.Observable

/**
 * Created by ltalavera on 3/14/18.
 * Handles the state of the activity/presenter
 */
class MainViewModel : ViewModel() {
    val user: User = User()
    var observable: Observable<User>? = null
}