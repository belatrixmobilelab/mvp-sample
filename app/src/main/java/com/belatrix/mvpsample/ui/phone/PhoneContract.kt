package com.belatrix.mvpsample.ui.phone

import com.belatrix.mvpsample.ui.base.BaseFragment
import com.belatrix.mvpsample.ui.base.BasePresenter

/**
 * Created by ltalavera on 2/23/18.
 * The contract for the registration activity/presenter
 */
interface PhoneContract {
    abstract class View : BaseFragment() {
        /**
         * Gets the phone number
         */
        abstract fun getPhoneNumber(): String

        /**
         * Displays the dialog to input the verification code
         * sent by message
         * @param phoneNumber If null, then dismiss dialog
         */
        abstract fun showSubmitCode(phoneNumber: String?)

        /**
         * Sets the submit code to the dialog
         * @param code The code received from the SMS
         */
        abstract fun setSubmitCode(code: String?)

        /**
         * Sets the UI as validated
         */
        abstract fun setValidatedUI()
    }

    abstract class Presenter<V : View> : BasePresenter<V>() {
        /**
         * Starts the validateAndVerifyPhoneNumber step of validation after verifying
         * that the number is correct
         */
        abstract fun validateAndVerifyPhoneNumber()
    }
}
