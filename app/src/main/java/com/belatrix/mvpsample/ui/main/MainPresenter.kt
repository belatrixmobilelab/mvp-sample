package com.belatrix.mvpsample.ui.main

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.ViewModelProviders
import com.belatrix.mvpsample.business.encryption.Crypto
import com.belatrix.mvpsample.managers.AuthManager
import com.belatrix.mvpsample.models.User
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by ltalavera on 3/14/18.
 * Implements the presenter for the mvp main
 */
class MainPresenter @Inject constructor(private val authManager: AuthManager, private var crypto: Crypto) : MainContract.Presenter<MainActivity>()  {
    private lateinit var viewModel: MainViewModel
    private var disposable: Disposable? = null

    private val authStateListener: AuthManager.AuthStateListener = object : AuthManager.AuthStateListener {
        override fun onAuthStateChanged(user: User?) {
            if (user != null) {
                getViewOrThrow().goToLogin()
            }
        }
    }

    override fun attach(view: MainActivity, lifecycle: Lifecycle) {
        super.attach(view, lifecycle)
        viewModel = ViewModelProviders.of(view).get(MainViewModel::class.java)
        disposable = subscribeTo(viewModel.observable)
    }

    override fun subscribe() {
        authManager.addAuthStateListener(authStateListener)
        disposable = subscribeTo(viewModel.observable)
    }

    override fun unsubscribe() {
        authManager.removeAuthStateListener(authStateListener)
        disposable?.dispose()
    }

    override fun prepareCurrentFragment() {

    }

    override fun encrypt(text: String) {
        val cipher = crypto.apply(text)
        getViewOrThrow().showText(cipher)
    }

    private fun dismissRegistration() {
        getViewOrThrow().showProgress(false)
        viewModel.observable = null
    }

    private fun subscribeTo(observable: Observable<User>?): Disposable? {
        viewModel.observable = observable
        return viewModel.observable?.subscribe({
            dismissRegistration()
        }, { error ->
            getViewOrThrow().showError(error.message)
            dismissRegistration()
        })
    }
}