package com.belatrix.mvpsample.ui.phone

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import javax.inject.Inject

/**
 * Created by ltalavera on 3/16/18.
 * Implements the phone presenter
 */
class PhonePresenter @Inject constructor(private val auth: FirebaseAuth, private val phoneAuth: PhoneAuthProvider) : PhoneContract.Presenter<PhoneFragment>() {
    private lateinit var phoneViewModel: PhoneViewModel

    override fun attach(view: PhoneFragment, lifecycle: Lifecycle) {
        super.attach(view, lifecycle)
        phoneViewModel = ViewModelProviders.of(getViewOrThrow()).get(PhoneViewModel::class.java)
    }

    override fun validateAndVerifyPhoneNumber() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
