package com.belatrix.mvpsample.ui.phone

import android.arch.lifecycle.ViewModel
import com.google.firebase.auth.PhoneAuthProvider

/**
 * Created by ltalavera on 3/16/18.
 * ViewModel to keep the state of the presenter
 */
class PhoneViewModel : ViewModel() {
    lateinit var phoneNumber: String
    lateinit var verificationId: String
    lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
}