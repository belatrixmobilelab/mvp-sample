package com.belatrix.mvpsample.ui.main

import com.belatrix.mvpsample.business.encryption.Crypto
import com.belatrix.mvpsample.business.injection.scope.ActivityScope
import com.belatrix.mvpsample.business.injection.scope.FragmentScope
import com.belatrix.mvpsample.managers.AuthManager
import com.belatrix.mvpsample.ui.phone.PhoneFragment
import com.belatrix.mvpsample.ui.phone.PhonePresenter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
@Suppress("unused")
abstract class MainModule {
    @ActivityScope
    fun mainPresenter(authManager: AuthManager, crypto: Crypto): MainPresenter {
        return MainPresenter(authManager, crypto)
    }

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun phoneFragment(): PhoneFragment

    @ActivityScope
    fun phonePresenter(auth: FirebaseAuth, phoneAuth: PhoneAuthProvider): PhonePresenter {
        return PhonePresenter(auth, phoneAuth)
    }
}