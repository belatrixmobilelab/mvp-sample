package com.belatrix.mvpsample.ui.base

/**
 * Created by ltalavera on 2/23/18.
 * Base for Views MVP
 */
interface BaseView {
    fun showProgress(show: Boolean)
    fun setProgressMessage(message: Int)
    fun showError(error: Int)
    fun showError(error: String?)
    fun toastError(error: Int)
    fun snackError(error: String)
}