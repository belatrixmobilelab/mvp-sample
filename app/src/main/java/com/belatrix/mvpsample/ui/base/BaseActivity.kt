package com.belatrix.mvpsample.ui.base

import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast
import com.belatrix.mvpsample.components.dialogs.DelayedProgressDialog
import com.belatrix.mvpsample.components.dialogs.MessageDialog
import com.belatrix.mvpsample.utils.UiUtils
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Created by ltalavera on 1/19/18.
 * The base activity with default functionality
 */
abstract class BaseActivity : BaseView, DaggerAppCompatActivity() {
    companion object {
        internal var TAG = BaseActivity::class.java.simpleName
    }

    @Inject lateinit var progressDialog: DelayedProgressDialog

    override fun showProgress(show: Boolean) {
        UiUtils.hideSoftKeyboard(currentFocus)

        if (show) {
            progressDialog.show(supportFragmentManager, TAG)
        } else {
            progressDialog.cancel()
        }
    }

    override fun setProgressMessage(message: Int) {
        progressDialog.setMessage(message)
    }

    override fun showError(error: Int) {
        MessageDialog.Builder(this)
                .setMessage(error)
                .show(supportFragmentManager, TAG)
    }

    override fun showError(error: String?) {
        MessageDialog.Builder(this)
                .setMessage(error)
                .show(supportFragmentManager, TAG)
    }

    override fun toastError(error: Int) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun snackError(error: String) {
        val root = findViewById<View>(android.R.id.content)
        Snackbar.make(root, error, Snackbar.LENGTH_LONG).show()
    }
}