package com.belatrix.mvpsample.components.dialogs

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.belatrix.mvpsample.R
import javax.inject.Inject
import javax.inject.Singleton

/*
 * Copyright 2017 Qi Li
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@Singleton
class DelayedProgressDialog @Inject constructor() : DialogFragment() {
    /** Defaults  */
    companion object {
        private const val DELAY_MILLISECOND: Long = 150
        private const val SHOW_MIN_MILLISECOND: Long = 300
    }

    /** UI  */
    private var progressBar: ProgressBar? = null
    private var tvMessage: TextView? = null

    /** Controllers */
    private var message: Int = R.string.text_loading
    private var startedShowing = false
    private var startMillisecond: Long = 0
    private var stopMillisecond: Long = 0
    private val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        isCancelable = false

        return AlertDialog.Builder(activity, R.style.SampleUI_ProgressDialog)
                .setView(View.inflate(activity, R.layout.dialog_progress, null))
                .create()
                .apply {
                    setCanceledOnTouchOutside(false)
                }
    }

    override fun onStart() {
        super.onStart()
        prepareUI()
    }

    override fun onDestroyView() {
        if (dialog != null && retainInstance) {
            dialog.setDismissMessage(null)
        }
        super.onDestroyView()
    }

    override fun show(fm: FragmentManager, tag: String) {
        message = R.string.text_loading
        startMillisecond = System.currentTimeMillis()
        startedShowing = false
        stopMillisecond = java.lang.Long.MAX_VALUE

        handler.postDelayed({
            if (stopMillisecond > System.currentTimeMillis()) {
                showDialogAfterDelay(fm, tag)
            }
        }, DELAY_MILLISECOND)
    }

    /**
     * Sets the message to the alert dialog
     * @param message The message to display
     */
    fun setMessage(message: Int) {
        this.message = message
        tvMessage?.setText(message)
    }

    /**
     * Stops the progress dialog
     */
    fun cancel() {
        stopMillisecond = System.currentTimeMillis()

        if (startedShowing) {
            if (progressBar != null) {
                cancelWhenShowing()
            } else {
                cancelWhenNotShowing()
            }
        }
    }

    private fun prepareUI() {
        // Setup UI
        progressBar = dialog.findViewById(R.id.progress_bar)
        tvMessage = dialog.findViewById(R.id.tvMessage)

        // Set message
        tvMessage?.setText(message)
    }

    private fun showDialogAfterDelay(fm: FragmentManager, tag: String) {
        startedShowing = true
        super.show(fm, tag)
    }

    private fun cancelWhenShowing() {
        if (stopMillisecond < startMillisecond + DELAY_MILLISECOND + SHOW_MIN_MILLISECOND) {
            handler.postDelayed({ this.dismissAllowingStateLoss() }, SHOW_MIN_MILLISECOND)
        } else {
            dismissAllowingStateLoss()
        }
    }

    private fun cancelWhenNotShowing() {
        handler.postDelayed({ this.dismissAllowingStateLoss() }, DELAY_MILLISECOND)
    }
}
