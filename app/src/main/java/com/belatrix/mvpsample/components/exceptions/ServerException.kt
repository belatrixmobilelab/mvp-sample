package com.belatrix.mvpsample.components.exceptions

/**
 * Created by ltalavera on 3/6/18.
 * Exception from the server
 */
data class ServerException(override var message: String) : RuntimeException(message)
